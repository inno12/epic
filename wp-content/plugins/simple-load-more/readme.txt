=== Simple Load More ===
Contributors: Webgensis
Tags: wordpress, plugin, simple load more , ajax, load more, svg, post load more, ajax load more
Requires at least: 3.9
Tested up to: 4.7
Stable tag: 1.0

Add a "Load More" button to dynamically add posts without pagination and page loads.

== Description ==

Simple Load More will add a button for ajax content loading. Works automatically on your front page, any taxonomy or custom post type page, any archive page, and search pages without special configuration.

== Installation ==

Installing "Simple Load More" can be done either by searching for "Simple Load More" via the "Plugins > Add New" screen in your WordPress dashboard, or by using the following steps:

1. Download the plugin via WordPress.org
2. Upload the ZIP file through the 'Plugins > Add New > Upload' screen in your WordPress dashboard
3. Activate the plugin through the 'Plugins' menu in WordPress

Once plugin is activated, you need to update plugin settings under 'Settings > Simple Load More'. On pages where you would like to use the button you need to add the "simple_more_button()" function OR [load_more button_text="ABC"] shortcode to your template files.

== Screenshots ==

1. Add a load more button to your posts loop.
2. Choose from 9 different animated loading icons.

== Changelog ==

= 1.0 =
* 2017-01-19
* Initial release
