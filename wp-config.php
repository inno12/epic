<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'EpicAviation');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         'CCv&L>8?x.^FKf6M^W(%mi/8~HrLO^VUi-|f)-$c?RZ{^5)%+,R}F|VpJW@Km0Hx');
 define('SECURE_AUTH_KEY',  '9*(v@.(EU]uZK*&KVhQ,-5ixh,.+eDxx8]Jx!+GAj,))0K0X6<9k>A1sDQ`)J% c');
 define('LOGGED_IN_KEY',    'Wl#W _bq_o=h]gQKWL,!4%:b*tLdJ+ZrJOV2^Aef!.O2mRJe:0.F*Vfl9AV]5ELZ');
 define('NONCE_KEY',        '6t)}M4P(n{+yk&D^FcMQUGp?A#@nuneGw>cYszDQ;-,Xon^N~4}$3TGZ|y=7|CQh');
 define('AUTH_SALT',        'Q5ph6w^Q{[NY_Q!f%?/WtD{ax66Kq]`j[F|FE^Otw<^^xRmD$srqQ>SH[:cBE5[|');
 define('SECURE_AUTH_SALT', 'EY$ ;->*=iJ`A@0U=-K3+{e:Xc+]mBauzd[;oS|VX#/GK-4Dg}ai;>Pe/^PF-dIr');
 define('LOGGED_IN_SALT',   'cff{wkm>@4HOa 8d6s-2VlNOyu)8We@TA_hY#};)+uE7ur5T 6LLMV_KC%+rYOIx');
 define('NONCE_SALT',       '`D&MxW*jcQ|rU^B%lvyOUI/9#UT2l98Y:wcRMiC v1AC=tej+Zi[wtNTqb+V3Fx2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
